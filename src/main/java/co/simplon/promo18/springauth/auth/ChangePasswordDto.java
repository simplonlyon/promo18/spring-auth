package co.simplon.promo18.springauth.auth;

public class ChangePasswordDto {
    public String oldPassword;
    public String newPassword;
}
