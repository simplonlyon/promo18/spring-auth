# Spring Authentication

Projet Spring contenant une API Rest avec compte utilisateur·ice / protection des routes etc.

## De quoi on a besoin

Dépendances utiliser dans ce projet : 
* Spring Security (obligatoire si on veut gérer l'authentification avec Spring), ici c'est la version 5.7, sortie récemment avec Spring Boot 2.7 (la plupart des tutos sont avec l'ancienne version, mais les concepts restent les mêmes)
* Spring data jpa, Mysql Driver (on peut utiliser jdbc à la place)
* Spring devtools et Spring Actuator (outils de débug et de dev)
* Spring Validation I/O (pour la validation à l'inscription du user)
* Spring Web (pour faire les routes/contrôleurs)

Pour une bon système d'authentification avec Spring boot, voici les différentes choses nécessaires :

### Une entité User
Une entité User indique ce qui persistera en base de données, en général on souhaitera un champ faisant office de d'identifiant (l'email c'est pas mal pour ça), un mot de passe, et si notre site à plusieurs rôles possibles, un role.

(Ici la classe s'appelle User, mais elle pourrait s'appeler n'importe comment, Student, Account, Customer, etc.)
```java
@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    @Size(min=4, max=64)
    private String password;
    private String role;
    
    //...
}
```

Pour être utilisable par Spring Security, l'entité doit obligatoirement implémenter l'interface UserDetails et toutes ses méthodes ([exemple d'implémentation](src/main/java/co/simplon/promo18/springauth/entity/User.java))

### Un Repository pour le User
Le repository pour l'entité User doit notamment permettre de récupérer un User spécifique par son identifiant.

Exemple avec un JpaRepository dans le cas où l'identifiant est l'email :
```java
@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    Optional<User> findByEmail(String email);
}
```

### Un UserDetailsService
Spring Security a également besoin d'une classe qui implémente l'interface UserDetailsService, qui indique à Spring comment récupérer un User au moment de la connexion.

Exemple avec une classe qu'on appelle AuthService, qu'on peut appeler comme on le souhaite, et qui utilise notre UserRepository :

```java
@Service
public class AuthService implements UserDetailsService{
    @Autowired
    private UserRepository repo;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        return repo.findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }  
}
```


### Fichier de configuration de Spring Security 
Cette classe, qui devra être annotée par un @Configuration devra contenir plusieurs parties ([configuration complète](src/main/java/co/simplon/promo18/springauth/auth/SecurityConfig.java))

#### La définition de l'algorithme de Hash
Indique quel PasswordEncoder sera utilisé dans toute l'application, ce qui permettra de hasher le mot de passe au moment de l'inscription dans le contrôleur, et au moment du login (fait par spring security)
```java
@Bean
public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(12);
}
```
Le @Bean, indique que c'est cette méthode qui sera utilisée pour récupérer une instance lorsqu'on fera un @Autowired de PasswordEncoder

#### La définition des access control
Les access controls représentent la liste des routes et qui y a accès, faut-il être connecté·e pour y accéder, faut-il avoir un rôle spécifique, etc.
```java
@Bean
public SecurityFilterChain filterChain(HttpSecurity http) throws Exception
{

    http.httpBasic();

    http.authorizeRequests()
        .mvcMatchers("/api/user/account").authenticated()
        // .mvcMatchers(HttpMethod.GET, "/api/user").authenticated()
        .anyRequest().permitAll()
        .and().csrf().disable();

    return http.build();
}
```
Ici, on utilise le mvcMatchers pour indiquer éventuellement la méthode ciblée, la ou les routes, et derrière qui a le droit d'accèder ou non à cette route.

Le anyRequest().permitAll() indique que toutes les routes restantes seront accessibles à tout le monde.

### Le contrôleur d'authentification

Ce contrôleur est un RestController classique qui contiendra une route permettant de s'inscrire, dans laquelle il faudra faire en sorte de hasher le mot de passe avant de faire persister le user.

Il contiendra également une route définie comme authenticated dans le access control, qui permettra de récupérer le user actuellement connecté et qui sevira de route de login

```java
@GetMapping("/account")
public User getAccount(@AuthenticationPrincipal User user) {
    return user;
}
```